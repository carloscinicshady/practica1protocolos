package com.practica1microservicios.server.infrastructure.inputPort;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

@Component
public class WebSocketInputPort extends AbstractWebSocketHandler {

    Logger myLogger = LoggerFactory.getLogger(WebSocketInputPort.class);

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {

        myLogger.info("Sentence to parse to caps: " + message.getPayload());
        session.sendMessage(new TextMessage("" + message.getPayload()));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        myLogger.info("Client disconnected " + session.getId());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        myLogger.info("Client connected " + session.getId());
    }

}
