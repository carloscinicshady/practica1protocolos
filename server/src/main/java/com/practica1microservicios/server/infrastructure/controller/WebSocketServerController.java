package com.practica1microservicios.server.infrastructure.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketServerController {
    @MessageMapping("/request")
    @SendTo("queue/responses")
    public String onMessage(String message){
        return "server response: "+message;
    }

    @MessageMapping("/subscribe")
    @SendTo("/queue/errors")
    public String subscribe() {
        return "Subscribed to /queue/errors";
    }



}
