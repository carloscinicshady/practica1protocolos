package com.practica1microservicios.server.domain.aggregate;

import java.util.UUID;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@Builder
@Getter
public class Task {
    String taskId;

    public Task() {
        this.taskId = UUID.randomUUID().toString();
    }
}
