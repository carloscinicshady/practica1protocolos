let stompClient = null;

function setConnected(connected) {
  $("#connect").prop("disabled", connected);
  $("#disconnect").prop("disabled", !connected);
  $("#send").prop("disabled", !connected);

  if (connected) {
    $("#conversation").show();
  } else {
    $("#conversation").hide();
  }

  $('#output').val('');
  $("#responses").html("");
}

function connect() {
  const socket = new SockJS('/websocket-server');
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) {
    setConnected(true);

    const subscription = stompClient.subscribe('/queue/responses', function (response) {
      showMsg(response, 'table-success');
    });

    stompClient.subscribe('/queue/errors', function (response) {
      showMsg(response, 'table-danger');
      console.log('Client unsubscribed:' + subscription);
      subscription.unsubscribe({});
    });

    stompClient.subscribe('/topic/periodic', function (response) {
      showMsg(response, 'table-info');
    });

    stompClient.subscribe('/app/task-id', function (response) {
      const taskId = response.body;
      showTaskId(taskId);
    });
  });
}

function disconnect() {
  if (stompClient == null) {
    stompClient.disconnect();
  }
  setConnected(false);
  console.log("Client Disconnected");
}

function sendMsg() {
  const output = $("#output").val();
  stompClient.send("/app/request", {}, output);
}

function createTask() {
  if (stompClient !== null && stompClient.connected) {
    const response = "Client has created a task";
    stompClient.send("/app/task-created", {}, response);
    showMsg(response, 'table-success');
  } else {
    console.log("Cannot create task. WebSocket connection not established.");
  }
}

function showMsg(response, clazz) {
  console.log("Client received: " + response);
  $("#responses").append("<tr class='" + clazz + "'><td>" + response + "</td></tr>");
}

function showTaskId(response) {
  const taskId = response.body;
  console.log("Received taskId: " + taskId);
  $("#responses").append("<tr><td>" + taskId + "</td></tr>");
}

$(function () {
  $("form").on('submit', (e) => { e.preventDefault(); });
  $("#connect").click(() => { connect(); });
  $("#disconnect").click(() => { disconnect(); });
  $("#createtask").click(() => { createTask(); });
});
