package com.practica1microservicios.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;

@SpringBootApplication
@EnableWebSocketMessageBroker
@ComponentScan(basePackages = "com.practica1microservicios.server.infrastructure.controller")
@ComponentScan(basePackages = "com.practica1microservicios.server.infrastructure.config")
public class ServerApplication {

	public static void main(String[] args) {

		SpringApplication.run(ServerApplication.class, args);
	}

}
