package com.practica1microservicios.server.infrastructure.controller;

import java.util.UUID;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.practica1microservicios.server.domain.aggregate.Task;

@Controller
public class TaskController {
    private final SimpMessagingTemplate messagingTemplate;

    public TaskController(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @MessageMapping("/task-created")
    public void receiveTask() {

        messagingTemplate.convertAndSendToUser("user", "/app/task-created", "Server -> Task has been received!");
    }

    @MessageMapping("/task-id")
    @SendTo("/app/task-id")
    public String handleTaskId(String taskId) {
        taskId = generateTaskId();
        System.out.println(taskId);
        messagingTemplate.convertAndSend("/app/task-id", taskId);
        return taskId;
    }

    private String generateTaskId() {
        return "1234";
    }
}
